extends Node

export var coins_on_level = 100
export(String) var level_name

var collected_coins = 0

func _ready():
	var coins = get_node("../Coins")
	var coins_child = coins.get_children()
	
	var i = 1
	for coin in coins_child:
		coin.id = i
		i += 1

func _process(delta):
	pass