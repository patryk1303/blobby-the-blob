extends Node

const DIGITS = 6

const STAMINA_BAR_MAX_WIDTH = 184

func render_collected_coins():
	var collected_coins = String(globals.collected_coins)
	var collected_coins_len = collected_coins.length()
	var diff = DIGITS - collected_coins_len
	var rendered_text = ""
	if(diff > 0):
		for i in range(0, diff):
			rendered_text += "0"
	rendered_text += collected_coins
	
	$GUI_Coin/Label.text = rendered_text
	
func render_stamina():
	var player = get_parent()
	var stamina = player.stamina
	var stamina_bar_width = globals.map(stamina, 0, 100, 0, STAMINA_BAR_MAX_WIDTH)

	$StaminaBar/StaminaBarMiddle.scale.x = stamina_bar_width / 4
	pass

func _ready():
	pass

func _process(delta):
	render_collected_coins()
	render_stamina()
	pass