extends Node

var mouse_on_start = false

func start_game():
	# reset globals
	globals.collected_coins = 0
	globals.collected_coins_arr = []
	# goto scene
	get_tree().change_scene("res://levels/LevelTest.tscn")
	pass

func _input(event):
	var start_pressed = Input.is_action_just_pressed("ui_start")
	
	if start_pressed:
		start_game()