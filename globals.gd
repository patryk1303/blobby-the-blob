extends Node

const GRAVITY = Vector2(0, 900)
const UP = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 5.0

const GROUP_ENEMIES = "enemies"

export var collected_coins = 0
export var collected_coins_arr = []

# form p5.js library
func map(n, start1, stop1, start2, stop2, within_bounds = false):
	var new_val = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2
	
	if not within_bounds:
		return new_val
		
	if start2 < stop2:
		return constrain(new_val, start2, stop2)
	else:
		return constrain(new_val, stop2, start2)
		
func constrain(n, low, high):
	return max(min(n, high), low)