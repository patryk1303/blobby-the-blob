extends Area2D

export(int, 1, 25, 1) var coin_value

var is_collected = false
var level_controller
var obj_collect

var id = -1

func collect(is_scene_load = false):
	is_collected = true
	if not is_scene_load:
		globals.collected_coins += coin_value
		globals.collected_coins_arr.append(obj_collect)
		level_controller.collected_coins += coin_value
		if not $AudioStreamPlayer2D.playing:
			$AudioStreamPlayer2D.play()

func _ready():
	level_controller = get_parent().get_parent().get_node("LevelController")
	obj_collect = level_controller.level_name + " " + String(id)

	if not coin_value:
		coin_value = 0
	
	if globals.collected_coins_arr.has(obj_collect):
		collect(true)
	
	pass

func _process(delta):
	if globals.collected_coins_arr.has(obj_collect) or is_collected:
		hide()
	pass

func _on_Coin_body_entered(body):
	if body is KinematicBody2D and (body.name == "Blobby" or body.name == "Blobby2") and not is_collected:
		collect(false)
