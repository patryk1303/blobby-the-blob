extends KinematicBody2D

export var max_speed = 380
export var max_run_speed = 550
export var jump_strength = -500
export var acceleration = 35
export var run_acceleration = 75

export(Texture) var backgroud_image
export(bool) var has_camera_bounds = true

const MAX_ATTACK_TIME = 150
const STOP_FORCE = 1000

# stamina
var stamina = 100

# dying
var is_alive = true
var die_timer = 5000

# attacking
var is_attacking = false
var attack_timer = MAX_ATTACK_TIME

# motion
var motion = Vector2()

# level bound and camera
var level_bounds = {
	top = -200,
	right = 6400,
	bottom = 1000,
	left = -1000
}

# animation
const ATTACK_ANIM = "attack"
var anim = "idle"
var new_anim = ""

func die():
	$sfx_hurt.play()
	is_alive = false
	
func play_anim():
	if new_anim != anim:
		anim = new_anim
		$Animation.play(anim, 0.1)
		
	if is_attacking or attack_timer > MAX_ATTACK_TIME:
		# play also attack animation
		$Animation.play(ATTACK_ANIM, 0.1)

func _ready():
	if backgroud_image:
		$Background/Background.texture = backgroud_image
	if has_camera_bounds:
		$Camera2D.limit_top = level_bounds.top
		$Camera2D.limit_right = level_bounds.right
		$Camera2D.limit_bottom = level_bounds.bottom
		$Camera2D.limit_left = level_bounds.left

func _physics_process(delta):
	var friction = false
	var acceleration_to_apply = acceleration
	var max_speed_to_apply = max_speed
	
	# store pressed action buttons
	var pressedLeft = Input.is_action_pressed("move_left")
	var pressedRight = Input.is_action_pressed("move_right")
	var pressedRun = Input.is_action_pressed("move_run")
	var pressedAttack = Input.is_action_just_pressed("move_attack")
	var pressedJump = Input.is_action_just_pressed("move_jump")
	var pressedAccept = Input.is_action_just_pressed("ui_accept")

	var fv = get_floor_velocity()

	motion += globals.GRAVITY * delta

	if not is_alive:
		new_anim = "dead"
		motion.x = lerp(motion.x, 0, 0.25)
		die_timer = lerp(die_timer, 0, 0.1)
		if pressedAccept or die_timer < 1.5:
			get_tree().reload_current_scene()
		motion = move_and_slide(motion, globals.UP, globals.SLOPE_SLIDE_STOP)
		play_anim()
		return

	if pressedRun and motion.x !=0:
		stamina -= 1
		if stamina > 0:
			acceleration_to_apply = run_acceleration
			max_speed_to_apply = max_run_speed
	else:
		stamina += 0.25

	stamina = clamp(stamina, 0, 100)

	if pressedRight:
		motion.x += acceleration_to_apply
		$Armature.scale.x = 1
		new_anim = "run"
	elif pressedLeft:
		motion.x -= acceleration_to_apply
		$Armature.scale.x = -1
		new_anim = "run"
	else:
		friction = true
		new_anim = "idle"

	motion.x = clamp(motion.x, -max_speed_to_apply, max_speed_to_apply)
	
	if is_on_floor():
#		motion += fv * delta
		if pressedJump and stamina > 45:
			motion.y = jump_strength
			stamina -= 20
			$sfx_jump.play()
			if get_floor_velocity().y < 0:
				position.y += -600 * delta
		if friction:
			motion.x = lerp(motion.x, 0, 0.25)
	else:
		if motion.y >= 0:
			new_anim = "fall"
		else:
			new_anim = "jump"
		if friction:
			motion.x = lerp(motion.x, 0, 0.05)
			
	if pressedAttack:
		attack_timer = MAX_ATTACK_TIME
		is_attacking = true
		
	if is_attacking or attack_timer > MAX_ATTACK_TIME:
		attack_timer = lerp(attack_timer, 0, 0.1)
#		new_anim = "attack"
		
		if attack_timer < 1.5:
			is_attacking = false

	motion = move_and_slide(motion, globals.UP, globals.SLOPE_SLIDE_STOP)	

	play_anim()

func _on_AttackCheck_body_entered(body):
	print(body.name)
	if body.is_in_group(globals.GROUP_ENEMIES):
		body.hit()
