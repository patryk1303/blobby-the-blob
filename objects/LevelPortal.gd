extends Area2D

export(String, FILE, "*.tscn") var target_level

func _on_LevelPortal_body_entered(body):
	if body is preload("res://objects/Blobby.gd"):
		print("is blobby")
		get_tree().change_scene(target_level)
